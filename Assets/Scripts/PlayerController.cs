﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class PlayerController : SimpleGameStateObserver, IEventHandler{

	[Header("Spawn")]
	[SerializeField]
	private Transform m_SpawnPoint;

	#region Physics gravity
	[SerializeField] Vector3 m_LowGravity;
	[SerializeField] Vector3 m_HighGravity;
	//Vector3 m_Gravity;
	#endregion

	[SerializeField]
	private float m_TranslationSpeed;

	protected override void Awake()
	{
		base.Awake();
		
	}

	private void Reset()
	{
		
	}

	// Use this for initialization
	void Start () {
		//m_Gravity = m_HighGravity;
	}

	private void Update()
	{
		//bool fire = Input.GetAxis("Fire1") > 0;
		////Debug.Log("Fire = " + fire);
		//m_Gravity = fire ? m_LowGravity : m_HighGravity;

	}

	// Update is called once per frame
	void FixedUpdate () {
		if (GameManager.Instance && !GameManager.Instance.IsPlaying) return;

		float hInput = Input.GetAxis("Horizontal");
		float vInput = Input.GetAxis("Vertical");
		bool jump = Input.GetAxis("Jump") > 0 || Input.GetKeyDown(KeyCode.Space);

		//m_Rigidbody.rotation = Quaternion.AngleAxis(90 * Mathf.Sign(hInput), Vector3.up);

		//m_Rigidbody.MovePosition(m_Rigidbody.position + m_Transform.forward * m_TranslationSpeed * hInput * Time.fixedDeltaTime);

	}

	private void OnCollisionEnter(Collision collision)
	{
		
	}

	private void OnCollisionExit(Collision collision)
	{

	}

	private void OnTriggerEnter(Collider other)
	{
		//if(GameManager.Instance.IsPlaying
		//	&& other.gameObject.CompareTag("Enemy"))
		//{
		//	if(other.GetComponent<Enemy>().IsEnemy)
		//		EventManager.Instance.Raise(new PlayerHasBeenHitEvent());
		//}
	}

	//Game state events
	protected override void GameMenu(GameMenuEvent e)
	{
		Reset();
	}
}